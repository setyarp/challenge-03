function filterCarByAvailability(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  console.log(cars);

  // Tempat penampungan hasil
  const result = [];

  // Tulis code-mu disini
  
  // Deklarasi variable untuk tempat penampungan
  let temp = 0;
  // Looping untuk mengecek array index
  for( let i=0; i < cars.length; i++){
    // Comparison operator untuk compare jika status available adalah true
      if (cars[i].available == true){
        // mengambil data cars dengan value available sama dengan true
        result[temp] = cars[i]
        // Memasukan data ke tempat penampungan
        temp++
      }
  }

  // Rubah code ini dengan array hasil filter berdasarkan availablity
  console.log(result)
  return result;
}
