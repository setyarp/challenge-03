function sortCarByYearDescendingly(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  console.log(cars);

  // Clone array untuk menghindari side-effect
  // Apa itu side effect?
  const result = [...cars];

  // Tulis code-mu disini

  //Looping untuk mengecek setiap array index dari index 0
  for (var i = 0; i < result.length; i++){
    // Lopping untuk sebelum index (perbandingan data)
  for (var j = 0; j < result.length-1; j++){
    // Algoritma bubble sort
    // Comparison operator untuk compare antara data index pertama lebih kecil dari data index setelahnya
    if(result[j].year < result[j + 1].year){
      // Membuat tempat menampung data (temporary)
      let temp = result[j]
      // Memasukan data yang lebih kecil ke temporary
      result[j] = result[j + 1]
      // Memasukkan data yang ada di temporary
      result[j + 1] = temp
    }
  }
}
  // Rubah code ini dengan array hasil sorting secara descending
  console.log(result)
  return result;
}